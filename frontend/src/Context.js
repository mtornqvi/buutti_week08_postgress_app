import React, { useState, createContext } from "react";
export const AppContext = createContext([{}, () => {}]);

export const ContextProvider = (props) => {
    const initialContext = {

        products : {},
        filteredProducts : {},
        singleproduct : {},
        categories : [],
        // NOTE : JSX to be rendered in a component can't be inside here!!
        //  productsJSX : {},
        // singleProductDisplay : false,

        loadingData: false,
        // modifying product
        modifyProductState: false,
    };
    const [state, setContextState] = useState(initialContext);
    return (
        <AppContext.Provider value={[state, setContextState]}>
            {props.children}
        </AppContext.Provider>
    );
};