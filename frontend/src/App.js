import { 
    Routes, 
    BrowserRouter as Router, 
    Route,
    Link
} from "react-router-dom";

import "./App.css";
import NotFound from "./pages/page-not-found/NotFound";
import homeIcon from "./assets/home-icon.png";
import Home from "./pages/home/Home";
import Products from "./pages/products/Products";
import React from "react";
import { ContextProvider } from "./Context";

function App() {
    return (
        <div className="main">
            <ContextProvider>
                <Router>
                    <nav>
                        <Link className="nav-home" to='/'>
                            <img 
                                src={homeIcon}
                                alt='Home-icon'
                                className="homeIcon"
                            ></img>
                        </Link>
                        <Link className='nav-link' to='/products'>Products</Link>                    
                    </nav>
                    <Routes>
                        <Route path='/' element={<Home/>}></Route>
                        <Route path='/products' element={<Products/>}></Route>
                        <Route path='*' element={<NotFound/>}></Route> 
                    </Routes>
                </Router>
            </ContextProvider>  
        </div>
    );
}

export default App;
