// https://reactjs.org/docs/handling-events.html

import React, { useContext, useEffect, useState, useRef} from "react";
import NewWindow from "react-new-window";
import Select from "react-dropdown-select";

import { getAllProducts, getAllCategories } from "../../services/getProducts.js";
import { AppContext } from "../../Context.js";
import "./Products.css";


const Products = () => {

    // Debugging
    // console.log("Starting products()");

    const hasFetchedData = useRef(false);
    const [state, setContextState] = useContext(AppContext);
    // NOTE: rendered JSX must be local
    const [productsJSX, setProductsJSX] = useState([]);
    // whether or not to display just a single product, must also be here
    const [singleProductDisplay, setSingleProductDisplay] = useState(false);

    let modifyProductJSX = "";

    // Debugging
    // console.log("Before updateInformation()");
    // console.log("Products:");
    // console.log(state.products);

    useEffect(  () => {
        const updateInformation = async () => {

            try {

                // fetch data
                // must use await!!
                const products = await getAllProducts();
                const categories = await getAllCategories();

                // set data to loading
                setContextState( (state) => {
                    return {...state,
                        loadingData:true,
                        products: products,
                        categories: categories,                    
                    };});                          

                // set loading to false (done)
                setContextState( (state) => {
                    return {...state,loadingData:false};});
               
            } catch (error) {
                console.log("Error in retrieving information");
            }
        };
        // check if data is fetched once, no need to update
        if (!hasFetchedData.current) {
            updateInformation();
            hasFetchedData.current = true;
        }
    // Warning message :
    // React Hook useEffect has a missing dependency: 'setContextState'. Either include it or remove the dependency array 
    // so must include this
    }, [setContextState]);

    // Debugging
    // console.log("updateInformation() done");
    // console.log("Products:");
    // console.log(state.products);

    const updateProductsJSX = (targetCategory) => {
  
        // Debugging
        // console.log("at updateProductsJSX()");
        // console.log("Selected category is :");
        // console.log(targetCategory);
        // console.log("Products are:");
        // console.log(state.products);

        let filteredProducts = state.products;
        if ( (targetCategory !== "all") ) {
            filteredProducts = state.products.filter( (product) => {
                return (product.category === targetCategory);
            });
        }

        // Debugging
        // console.log("Filtered products are:");
        // console.log(filteredProducts);


        const JSX = filteredProducts.map( (product) => {
            return (
                <div key={product.id} onClick={() => {
                    setContextState( (state) =>  {
                        return {...state, singleProduct : product};
                    });
                    setSingleProductDisplay(true);
                }}> 
                    <img src={product.image} alt="Product pic" width="80" height="80"/> <br/> 
                    {product.title} <br/>
                    {product.price} €
                </div>
            );
        });

        // Debugging
        // console.log("Trying to update Products JSX");
        setProductsJSX(JSX);

        // console.log("Done");
    };

    // update products JSX if switching back from single product display
    useEffect( () => {

        if (state.products.length !== undefined) {
            updateProductsJSX("all");
        }
    }, [singleProductDisplay, state.products.length]);



    // Debugging
    // console.log("initializing graphics");
    // console.log(`state.loadingData = ${state.loadingData}.`);
    // console.log(`state.products.length = ${state.products.length}.`);

    if (state.loadingData || state.products.length === 0 || state.categories.length === 0) {

        // Debugging
        // console.log("Stage I : at loading stage");

        return  (
            <div className="products-main">
                <h1> Loading data.... </h1>
                <p>  Please wait</p>
            </div>
        );
    } else if (!singleProductDisplay) {
        

        // Debugging
        // console.log("Stage II : trying to display products");
        // console.log("productsJSX");
        // console.log(productsJSX);
        // console.log("productsJSX.length");
        // console.log(productsJSX.length);

        // Make sure there is something to display (asynchronous loading)
        if (productsJSX.length === 0) {
            
            // Debugging
            // console.log("Updating view");
            
            updateProductsJSX("all");
        }

        // Debugging
        // console.log("Products JSX set.");
        // console.log("Setting categories.");

        // default option is all, then add the rest of the loaded categories
        const categoriesOptions = [];
        categoriesOptions[0] = {label: "all", value: "all"};
        for (let i=0; i< state.categories.length;i++) {
            categoriesOptions[i+1] = {label: state.categories[i], value: state.categories[i]};
        }

        return (
            <div className="products-main">
                <div className="shoptitle">
                    <h1>FakeStore</h1>
                </div>
                <div className="products-dropdown">
                    <Select options={categoriesOptions} dropdownPosition="auto" onChange={(values) => updateProductsJSX(values[0].value)}/>
                </div>
                <div className="products-wrapper"> 
                    {productsJSX}
                </div>
            </div>
        );
    } else {
        if (state.modifyProductState) {


            const updateSingleProduct = (e) => {
                // prevent submit from reloading
                e.preventDefault();
                const singleProduct = {
                    category: e.target.category.value,
                    description: e.target.description.value,
                    id: state.singleProduct.id,
                    image: state.singleProduct.image,
                    price: e.target.price.value,
                    rating: {"count": state.singleProduct.rating.count, "rate":e.target.rating.value},
                    title: e.target.title.value,
                };


                // Debugging
                // console.log(singleProduct.id);
                // console.log(state.singleProduct);

                // in the Fakestore API, id:s start from 1 => corresponds to 0
                const index = singleProduct.id - 1;
                const products = state.products;
                products[index] = singleProduct;

                // Update changes 
                setContextState( (state) => {
                    return {...state, products : products, singleProduct: singleProduct} ;                  
                });

                // Debugging
                // console.log("updateSingleProduct() ended");
            };

            // Debugging
            // console.log(`Description lenght is : ${state.singleProduct.description.length}`);
            const rowsNeeded = Math.ceil(state.singleProduct.description.length/50) + 1 ;

            // NOTE: Must use defaultValue, using value=> form can't be changed
            modifyProductJSX = (
                <NewWindow title="Modify product">
                    <h1>Modifying product</h1>
                    <form onSubmit={updateSingleProduct}>
                        <table className="products-table"> 
                            <tbody>
                                <tr> 
                                    <td> &nbsp; </td> 
                                    <td> <img src={state.singleProduct.image} alt="Product pic" width="80" height="80"/> </td>
                                </tr>
                                <tr>
                                    <td> title </td> 
                                    <td>  <input type="text" id="title" size="50" defaultValue={state.singleProduct.title}                                   
                                    /></td>
                                </tr>
                                <tr>
                                    <td> description </td> 
                                    <td>  <textarea id="description" rows={rowsNeeded} cols="50" defaultValue={state.singleProduct.description}                                   
                                    /></td>                                   
                                </tr>
                                <tr>
                                    <td> category </td> 
                                    <td>  <input type="text" id="category" size="50" defaultValue={state.singleProduct.category}                                   
                                    /></td>                                   
                                </tr>
                                <tr>
                                    <td> rating </td> 
                                    <td>  <input type="text" id="rating" size="50" defaultValue={state.singleProduct.rating.rate}                                   
                                    /></td>   
                                </tr>
                                <tr>
                                    <td> price </td> 
                                    <td>  <input type="text" id="price" size="50" defaultValue={state.singleProduct.price}                                   
                                    /></td>  
                                </tr>
                            </tbody>
                        </table>
                        <button className="" type="submit"> Submit changes
                        </button>
                    </form>

                </NewWindow>
            );
        }

        return (
            <div className="products-main">
                <h1> Now in Single product display. </h1>
                <button onClick={ () => {
                    setSingleProductDisplay(false);
                }}> Go back to all products display. </button>
                <table className="products-table"> 
                    <tbody>
                        <tr> 
                            <td> &nbsp; </td> 
                            <td> <img src={state.singleProduct.image} alt="Product pic" width="80" height="80"/> </td>
                        </tr>
                        <tr>
                            <td> title </td> 
                            <td>  {state.singleProduct.title} </td>
                        </tr>
                        <tr>
                            <td> description </td> 
                            <td>  {state.singleProduct.description} </td>
                        </tr>
                        <tr>
                            <td> category </td> 
                            <td>  {state.singleProduct.category} </td>
                        </tr>
                        <tr>
                            <td> rating </td> 
                            <td>  {state.singleProduct.rating.rate} </td>
                        </tr>
                        <tr>
                            <td> price </td> 
                            <td>  {state.singleProduct.price} € </td>
                        </tr>
                    </tbody>
                </table>
            
                <button onClick={ () => {
                    setContextState( (state) => {
                        return {...state,modifyProductState:true};}); 
                }}
                > Modify the product
                </button>

                <button onClick={ () => {
                    const filteredProducts = state.products.filter( (product) => {
                        return (product.id !== state.singleProduct.id);
                    });
                    setContextState( (state) =>  {
                        return {...state, products:filteredProducts};
                    });
                    setSingleProductDisplay(false);
                }}
                > Delete the product
                </button>

                {modifyProductJSX}
            </div>

        );


    }

};

export default Products;