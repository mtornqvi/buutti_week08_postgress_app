// Refer to : https://www.postgresql.org/docs/13/datatype.html
// version 13

import dotenv from "dotenv"; // for passwords
process.env.NODE_ENV != "prod" && dotenv.config();

const createProductsTable = `
    CREATE TABLE IF NOT EXISTS "products" (
	    "id" VARCHAR(36) NOT NULL,
		"image" VARCHAR(100) NOT NULL,
	    "title" VARCHAR(100) NOT NULL,
		"description" VARCHAR(100) NOT NULL,
		"category" VARCHAR(100) NOT NULL,
		"rating" VARCHAR(100) NOT NULL,
	    "price" INTEGER NOT NULL,
	    PRIMARY KEY ("id")
    );`;


const createCustomerTable = `
    CREATE TABLE IF NOT EXISTS "customer" (
	    "id" VARCHAR(36) NOT NULL UNIQUE,
	    "username" VARCHAR(100) NOT NULL UNIQUE,
	    "passhash" VARCHAR(100) NOT NULL,
	    PRIMARY KEY ("id")
    );`;


export default { 
    createProductsTable,  createCustomerTable
};