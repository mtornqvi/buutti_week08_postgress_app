import productDao from "../dao/productDao.js";
import fs from "fs";
const jsonFILE = "products.json";

const findAll = async () => {
    const products = await productDao.findAll();
    return products.rows;
};

export const initializeAllProducts = async () => {

    console.log("Initializing all products...");

    // Debugging
    // const files = fs.readdirSync("./");
    // console.log(files);

    const rawdata = fs.readFileSync(jsonFILE);
    const data = JSON.parse(rawdata);

    // Debugging
    // console.log(data);

    // map old format into more simple format
    const products = data.map( (element) => {
        const product = {
            id: element.id,
            title: element.title,
            price: element.price,
            description: element.description,
            image: element.image,
            rating: element.rating.rate,
        };

        return product;
    });

    initializeAllProducts(products);
}

export default { 
    findAll
};