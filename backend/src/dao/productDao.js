// Refer to : https://www.postgresql.org/docs/13/datatype.html
// version 13


import db from "../db/db.js";

const findAll = async () => {
    console.log("Requesting for all products...");
    const result = await db.executeQuery(
        "SELECT * FROM \"products\";"
    );
    console.log(`Found ${result.rowCount} products.`);
    return result;
};

const initializeAllProducts = async (products) => {

    const values = {};
    let i=0;
    products.array.forEach( (product) => {
        values[i++] = [...Object.values(product)];
    });    

    console.log("Inserting products...");
    const query = {
        text: "INSERT INTO \"products\" (\"id\",\"title\",\"price\",\"description\",\"category\",\"image\", \"rating\") VALUES ($1,$2,$3,$4,$5,$6,$7)",
        values: values,
    };
    console.log([...Object.values(product)]);
    const result = await db.executeQuery(query);
    console.log("Intial products inserted successfully.");

    return result;
};

export default {
    findAll, initializeAllProducts
};